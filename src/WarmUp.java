import java.util.Scanner;

/**
 * Данный класс Разминка создан для того чтобы продемонстрировать быстрое
 * "умножение без знака умножения"
 *
 * @author Komissarova M.E. 16IT18K
 */
public class WarmUp {
    /**
     * Scanner сделан статическим, так как не нужно будет его закрывать вызывая Scanner.close().
     */
    private static Scanner scanner = new Scanner(System.in);

    /**
     * Так как циклы с множественными уровнями вложенности лишь замедляют работу
     * были использованны обычные "цЫклы"
     * Как говорят великие умы - "Цикл он и в Африке цыкл" PS: Kirill I.
     */

    public static void main(String[] args) {
        System.out.print("Введите 2 числа.");
        int d = scanner.nextInt();
        int c = scanner.nextInt();
        int composition = multiply(c, d);
        System.out.printf("Произведение чисел %d * %d = %d \n", d, c, composition);

    }

    /**
     * Метод multiply получает на вход два целочисленных значения с и d, а возвращает произведение исходных значений.
     * Для начала выполняется проверка на наличие среди входных значений хотя бы одного 0. При наличии 0 метод возвращает 0.
     * В противном случае с помощью метода класса Math находится модули числа.
     * Далее находится минимальное и максимальное значение сравнением модулей исходных данных.
     * Потом последовательно выполняется сложение большего из двух чисел с самим собой несколько раз.
     * Кол-во повторений операции сложения определяется минимальным из двух операндов.
     * Потом происходит проверка на отрицательное знаение входные данные, при истинности условия выполнится замена
     * пололожитнльного значения произведения на отрицательное.
     *
     * @param c первый операнд
     * @param d второй операнд
     * @return composition
     */
    static int multiply(int c, int d) {
        int modC = c;
        int modD = d;
        int max;
        int min;
        if ((d == 0) || (c == 0)) {
            return 0;
        } else {
            if (c < 0) {
                modC = -c;
            }
            if (d < 0) {
                modD = -d;
            }
        }
        if (modD > modC) {
            max = modD;
            min = modC;
        } else {
            max = modC;
            min = modD;
        }
        int composition = max;
        for (int i = 1; i < min; i++) {
            composition += max;
        }
        if ((d < 0 && c > 0) || (c < 0 && d > 0)) {
            composition = -composition;
        }
        return composition;
    }
}
