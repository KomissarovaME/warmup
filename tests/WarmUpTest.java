import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class WarmUpTest {
    /*
     *Тест для проверки
     */
    @Test
    void testMultiply() {
        for (int c = -1_000; c < 1_000; c++) {
            for (int d = -1_000; d < 1_000; d++) {

                int result = WarmUp.multiply(c, d);
                assertEquals(c * d, result);
            }
        }
    }

    /*
     *Тест для проверки на работоспособность программы с умножением 0 и 0.
     */
    @Test
    void testMultiplyZeros() {
        assertEquals(0, WarmUp.multiply(0, 0));
    }
    /*
     *Тест для проверки на работоспособность программы с умножением 1 и 1.
     */
    @Test
    void testMultiplyPozitive() {
        assertEquals(1, WarmUp.multiply(1, 1));
    }
    /*
     *Тест для проверки на работоспособность программы с умножением -1 и 1.
     */
    @Test
    void testMultiplyNegativeAndPozitive0() {
        assertEquals(-1, WarmUp.multiply(-1, 1));
    }
    /*
     *Тест для проверки на работоспособность программы с умножением -1 и -1.
     */
    @Test
    void testMultiplyNegative() {
        assertEquals(1, WarmUp.multiply(-1, -1));
    }
    /*
     *Тест для проверки на работоспособность программы с умножением 0 и 1.
     */
    @Test
    void testMultiplyZeros0() {
        assertEquals(0, WarmUp.multiply(1, 0));
    }
    /*
     *Тест для проверки на работоспособность программы с умножением 0 и -1.
     */
    @Test
    void testMultiplyZeros1() {
        assertEquals(0, WarmUp.multiply(0, -1));
    }


}